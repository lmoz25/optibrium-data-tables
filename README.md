# Optibrium Data Tables

Created for Optibrium coding challenge.

To build: run `build.sh`

To run tests: after build, run bin/tests

To do both, run `build-run-tests.sh`

To see docs: navigate to build/docs/html/index.html
