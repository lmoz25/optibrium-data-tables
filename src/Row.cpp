/// @file Row.cpp
#include "Row.h"

Row::Row(Column::Set& columns) {
    for (auto& column : columns) {
        AddColumn(column);
    }
}

bool Row::AddColumn(const Column& column) {
    auto success = data.emplace(column, Entry()).second;
    return success;
}

void Row::AddColumns(const Column::Set& columns) {
    for (auto& column : columns) {
        AddColumn(column);
    }
}

std::optional<Column> Row::GetColumn(const std::string& columnName) {
    auto res = std::find_if(data.begin(), data.end(),
        [&](const auto& it) -> bool {return it.first.name == columnName;});

    if (res == data.end()) {
        return std::optional<Column>();
    }

    return res->first;
}
Row::Entry Row::GetEntry(const std::string& columnName) {
    auto column = GetColumn(columnName);
    if(!column) {
        return Entry();
    }
    return data.find(column.value())->second;
}

bool Row::RemoveColumn(const std::string& columnName) {
    auto column = GetColumn(columnName);
    if(!column) {
        return false;
    }
    return data.erase(column.value());
}

bool Row::RemoveEntry(const std::string& columnName) {
    auto entry = GetEntry(columnName);
    if(!entry) {
        return false;
    }
    Column column{columnName, Column::Type::NONE};
    data.find(column)->second = Entry();
    return true;
}

Row::Map Row::MapUnion(const Row::Map& map1, const Row::Map& map2)  {
    Row::Map returnMap;
    returnMap.insert(map1.begin(), map1.end());

    for (auto& mapIter : map2) {
        // Try to add a name/row pair from the second row map.
        // emplace will return false if pair already exists.
        auto clash = !(returnMap.emplace(mapIter).second);
        if(clash) {
            // If clash, combine the two rows
            Row oldRow1 = returnMap.find(mapIter.first)->second;
            Row oldRow2 = mapIter.second;
            auto newRow = Row::merge(oldRow1, oldRow2);
            returnMap.insert_or_assign(mapIter.first, newRow);
        }
    }
    return returnMap;
}

Row Row::merge(const Row& row1, const Row& row2) {
    Row newRow = row1;
    for (auto& colEntryPair : row2.data) {
        // If row2 has a value, use it.
        // This is not good if the same row/column pair has a different value in two different
        // tables, but I'm not sure how to deal with that properly without an error prompt and user
        // intervention
        if(colEntryPair.second){
            newRow.data.insert_or_assign(colEntryPair.first, colEntryPair.second);
        }
    }
    return newRow;
}

Row::Map Row::MapIntersection(const Row::Map& map1, const Row::Map& map2) {
    Row::Map returnMap;
    for (auto& rowPair : map1) {
        if(map2.contains(rowPair.first)) {
            auto commonRow = map2.find(rowPair.first)->second;
            Row newRow;
            // If map2 has a row with the same name as map1, create a row containing the entries that the two rows have
            // in common
            for (auto& colValPair : commonRow.data) {
                if(rowPair.second.data.contains(colValPair.first)
                    && rowPair.second.data.find(colValPair.first)->second == colValPair.second) {
                        newRow.data.emplace(colValPair);
                }
            }
            if(!newRow.data.empty()) {
                returnMap.emplace(rowPair.first, newRow);
            }
        }
    }
    return returnMap;
}


template<>
bool Row::typeMatchesColumnType<int>(const Column::Type& type) {
    return type == Column::Type::INTEGER;
}

template<>
bool Row::typeMatchesColumnType<double>(const Column::Type& type) {
    return type == Column::Type::DECIMAL;
}

template<>
bool Row::typeMatchesColumnType<std::string>(const Column::Type& type) {
    return type == Column::Type::STRING;
}

