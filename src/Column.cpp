/// @file Column.cpp
#include "Column.h"

Column::Set Column::SetUnion(
    const Column::Set& set1, const Column::Set& set2
    ) {
    Column::Set returnSet;
    returnSet.insert(set1.begin(), set1.end());
    returnSet.insert(set2.begin(), set2.end());
    return returnSet;
}

Column::Set Column::SetIntersection(
    const Column::Set& set1, const Column::Set& set2
    ) {
    Column::Set returnSet;
    for (auto& column : set1) {
        // We want columns to have the same type for this.
        if (set2.contains(column)
            && column.type == set2.find(column)->type) {
            returnSet.emplace(column);
        }
    }
    return returnSet;
}