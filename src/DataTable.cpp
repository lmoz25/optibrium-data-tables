/// @file DataTable.cpp
#include <algorithm>

#include "DataTable.h"


DataTable::DataTable(Column::Set& columns, Row::Map& rows) 
: columns(columns), rows(rows) {
    for (auto& column : columns) {
        for (auto& row : rows) {
            row.second.AddColumn(column);
        }
    }
}

bool DataTable::AddColumn(const Column& column) {
    auto success = columns.emplace(column).second;
    if (!success) {
        return false;
    }
    for (auto& row : rows) {
        success = row.second.AddColumn(column);
        if(!success){
            return false;
        }
    }
    return true;
}

bool DataTable::AddRow(const std::string& name) {
    Row row{columns};
    auto success = rows.emplace(name, row).second;
    return success;
}

std::optional<Row> DataTable::GetRow(const std::string& name) const {
    auto rowIter = rows.find(name);
    if (rowIter == rows.end()) {
        return std::optional<Row>{};
    }
    return std::optional<Row>(rowIter->second);
}

std::optional<Column> DataTable::GetColumn(const std::string& name) const {
    for (auto& column : columns) {
        if (column.name == name) {
            return std::optional<Column>(column);
        }
    }
    return std::optional<Column>();
}

bool DataTable::RemoveRow(const std::string& name) {
    return rows.erase(name);
}

bool DataTable::RemoveColumn(const std::string& name) {
    for (auto& column : columns) {
        if(column.name == name) {
            for(auto& row : rows){
                row.second.RemoveColumn(name);
            }
            return columns.erase(column);
        }
    }
    return false;
}

bool DataTable::RemoveEntry(const std::string& columnName, const std::string& rowName) {
    auto entry = GetEntry(columnName, rowName);
    if (!entry) {
        return false;
    }

    return rows.find(rowName)->second.RemoveEntry(columnName);
}

Row::Entry DataTable::GetEntry(const std::string& columnName, const std::string& rowName) {
    auto row = GetRow(rowName);
    if (!row) {
        return Row::Entry();
    }
    return row.value().GetEntry(columnName);
}

DataTable DataTable::Union(const DataTable& table1, const DataTable& table2) {
    Column::Set newColumns = Column::SetUnion(table1.columns, table2.columns);

    auto rows1 = table1.rows;
    for (auto& row : rows1) {
        row.second.AddColumns(table2.columns);
    }
    auto rows2 = table2.rows;
    for (auto & row : rows2) {
        row.second.AddColumns(table1.columns);
    }
    
    Row::Map newRows = Row::MapUnion(rows1, rows2);
    return DataTable{newColumns, newRows};
}

DataTable DataTable::Intersection(const DataTable& table1, const DataTable& table2) {
    auto newColumns = Column::SetIntersection(table1.columns, table2.columns);
    auto newRows = Row::MapIntersection(table1.rows, table2.rows);
    return DataTable{newColumns, newRows};
}

std::vector<std::tuple<Row, Row, Difference>> DataTable::Diff(const DataTable& table1, const DataTable& table2) {
    auto rows1 = table1.GetRows();
    auto rows2 = table2.GetRows();

    std::vector<std::tuple<Row, Row, Difference>> differences;
    auto appendToDifferences = [&](const Row& r1, const Row& r2) {
        Difference diff{r1, r2};
        if(!diff.RowsEqual()) {
            differences.emplace_back(
                std::tuple<Row, Row, Difference>{r1, r2, diff});
        }
    };

    for (auto& rowPair : rows1) {
        auto row2 = table2.GetRow(rowPair.first);
        Row rowToDiff;
        if(row2) {
            rowToDiff = row2.value();
            rows2.erase(rowPair.first);
        }
        appendToDifferences(rowPair.second, rowToDiff);
    }
    for (auto& rowPair : rows2) {
        Row emptyRow{};
        appendToDifferences(emptyRow, rowPair.second);
    }

    return differences;
}