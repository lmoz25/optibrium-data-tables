/// @file Difference.cpp

#include "Difference.h"

Difference::Difference(const Row& r1, const Row& r2){
    rowsEqual = (r1 == r2);
    if(!rowsEqual) {
        findDifferences(r1, r2);
    }
}

void Difference::findDifferences(const Row& r1, const Row& r2) {
    // Copy over row2, so that we can keep track of the entries we diff.
    Row row = r2;
    for (auto& entry : r1.data) {
        if(!row.data.contains(entry.first)) {
            differences.emplace_back(std::pair<Row::Entry, Row::Entry>(entry.second, Row::Entry()));
        }
        else {
            // If row2 contains the entry, we remove it from our copy of the row so it isn't added twice.
            auto entry2 = row.data.find(entry.first);
            if (entry.second != entry2->second) {
                differences.emplace_back(std::pair<Row::Entry, Row::Entry>(entry.second, entry2->second));
                row.data.erase(entry.first);
            }
        }
    }
    // Add all the entries that weren't present in row1
    for (auto& entry : row.data) {
        if(!r1.data.contains(entry.first)) {
            differences.emplace_back(
                std::pair<Row::Entry, Row::Entry>(Row::Entry(), entry.second));
        }
    }
}