#include <catch/catch.hpp>
#include <iostream>

#include "DataTable.h"
#include "CommonTestData.h"

static std::string rowName{"Caffeine"};

DataTable setupTest() {
    auto columns = std::unordered_set<Column>({test::column1, test::column2, test::column3});
    Row row(columns);
    auto rows = Row::Map{{rowName, row}};
    DataTable table(columns, rows);
    return table;
}

TEST_CASE("Adding columns to the table") {
    auto table = setupTest();
    SECTION("Add new column") {
        Column column{"Boiling point", Column::Type::DECIMAL};
        REQUIRE(table.AddColumn(column));
    }
    SECTION("Try to add existing column") {
        REQUIRE(!table.AddColumn(test::column1));
    }
}

TEST_CASE("Adding rows to the table") {
    auto table = setupTest();
    std::string newRow("Paracetamol");
    SECTION("Add new row") {
        REQUIRE(table.AddRow(newRow));
    }
    SECTION("Try to add existing row") {
        REQUIRE(!table.AddRow(rowName));
    }
}

TEST_CASE("Adding entries to the table") {
    auto table = setupTest();

    SECTION("Add decimal entry") {
        REQUIRE(table.AddEntry("Solubility", "Caffeine", 5.05));
    }

    SECTION("Add string entry") {
        std::string entry("White");
        REQUIRE(table.AddEntry("Colour", "Caffeine", entry));
    }

    SECTION("Add integer entry") {
        REQUIRE(table.AddEntry("Molecular Weight", "Caffeine", 194));
    }
}

TEST_CASE("Test GetRow function") {
    auto table = setupTest();
    auto columns =
        std::unordered_set<Column>({test::column1, test::column2, test::column3});
    Row row1(columns);

    auto getTest = [&](Row& row) {
        auto row2 = table.GetRow(rowName);
        REQUIRE(row2);
        REQUIRE(row == row2.value());
    };

    SECTION("Get row") {
        getTest(row1);
    }

    SECTION("Get row with entries in it") {
        double entry = 15.3;
        REQUIRE(table.AddEntry(test::column1.name, rowName, entry));
        auto row3 = row1;
        row3.AddEntry(test::column1, entry);

        getTest(row3);
    }

    SECTION("Try get non-existent row") {
        REQUIRE(!table.GetRow("Indomethacin"));
    }
}

TEST_CASE("Test GetColumns function") {
    auto table = setupTest();
    auto columns =
        std::unordered_set<Column>({test::column1, test::column2, test::column3});
    REQUIRE(table.GetColumns() == columns);
}


TEST_CASE("Test GetEntry function") {
    auto table = setupTest();

    double solubility = 15.3;
    REQUIRE(table.AddEntry(test::column1.name, rowName, solubility));

    auto entry = table.GetEntry(test::column1.name, rowName);
    REQUIRE(entry);
    REQUIRE(std::get<double>(entry.value()) == solubility);
}

TEST_CASE("Test RemoveColumn function") {
    auto table = setupTest();
    SECTION("Remove existing column") {
        REQUIRE(table.GetColumn(test::column1.name));
        auto rows = table.GetRows();
        for (auto& row : rows){
            REQUIRE(row.second.GetColumn(test::column1.name));
        }

        REQUIRE(table.RemoveColumn(test::column1.name));
        REQUIRE(!table.GetColumn(test::column1.name));
        rows = table.GetRows();
        for (auto& row : rows){
            REQUIRE(!row.second.GetColumn(test::column1.name));
        }
    }

    SECTION("Try remove non-existent column") {
        REQUIRE(!table.GetColumn("F"));
        REQUIRE(!table.RemoveColumn("F"));
    }
}

TEST_CASE("Test RemoveRow function") {
    auto table = setupTest();
    SECTION("Remove existing row") {
        REQUIRE(table.GetRow(rowName));
        REQUIRE(table.RemoveRow(rowName));
        REQUIRE(!table.GetRow(rowName));
    }

    SECTION("Try remove non-existent row") {
        REQUIRE(!table.RemoveRow("String"));
    }
}

TEST_CASE("Test RemoveEntry function") {
    auto table = setupTest();
    SECTION("Remove existing entry") {
        REQUIRE(table.AddEntry(test::column1.name, rowName, 4.3));
        REQUIRE(table.GetEntry(test::column1.name, rowName));
        REQUIRE(std::get<double>(table.GetEntry(test::column1.name, rowName).value()) == 4.3);
        REQUIRE(table.RemoveEntry(test::column1.name, rowName));
        REQUIRE(!table.GetEntry(test::column1.name, rowName));
    }

    SECTION("Try remove non-existent entry") {
        REQUIRE(!table.GetEntry(test::column1.name, rowName));
        REQUIRE(!table.RemoveEntry(test::column1.name, rowName));
    }
}

TEST_CASE("Test Union function") {
    double caffeineSolubility = 5.05;
    int caffeineWeight = 194;
    auto table1 = setupTest();
    REQUIRE(table1.AddEntry(test::column1.name, rowName, caffeineSolubility));
    std::string caffeineColour{"White"};
    REQUIRE(table1.AddEntry(test::column2.name, rowName, caffeineColour));
    REQUIRE(table1.AddEntry(test::column3.name, rowName, caffeineWeight));
    Column column{"Melting point", Column::Type::DECIMAL};
    Column::Set columns{{column}};
    Row row{columns};
    auto columns2 = std::unordered_set<Column>({test::column1, test::column2, test::column3});
    auto compColumns = Column::SetUnion(columns, columns2);

    SECTION("Union of two unrelated tables") {
        Row::Map rows{{"Phenylalanine", row}};
        DataTable table2{columns, rows};
        double plineMeltingPoint = 541.0;
        REQUIRE(table2.AddEntry("Melting point", "Phenylalanine", plineMeltingPoint));

        auto table3 = DataTable::Union(table1, table2);

        REQUIRE(table3.GetColumns() == compColumns);
        
        Row compRow1{compColumns};
        Row compRow2{compColumns};

        REQUIRE(compRow1.AddEntry(test::column1, caffeineSolubility));
        REQUIRE(compRow1.AddEntry(test::column2, caffeineColour));
        REQUIRE(compRow1.AddEntry(test::column3, caffeineWeight));
        REQUIRE(compRow2.AddEntry(column, plineMeltingPoint));

        Row::Map compMap{{rowName, compRow1},
                        {"Phenylalanine", compRow2}};

        REQUIRE(table3.GetRows() == compMap);
    }

    SECTION("Union of two tables that share a row") {
        Row::Map rows{{rowName, row}};
        DataTable table2{columns, rows};
        double caffeineMeltingPoint = 460.0;
        REQUIRE(table2.AddEntry("Melting point", rowName, caffeineMeltingPoint));
        REQUIRE(
            std::get<double>(table2.GetEntry("Melting point", rowName).value())
                == caffeineMeltingPoint);

        auto table3 = DataTable::Union(table1, table2);

        REQUIRE(table3.GetColumns() == compColumns);
        Row compRow{compColumns};

        REQUIRE(compRow.AddEntry(test::column1, caffeineSolubility));
        REQUIRE(compRow.AddEntry(test::column2, caffeineColour));
        REQUIRE(compRow.AddEntry(test::column3, caffeineWeight));
        REQUIRE(compRow.AddEntry(column, caffeineMeltingPoint));

        auto testRow = table3.GetRow(rowName);
        REQUIRE(testRow);
        REQUIRE(testRow.value() == compRow);
    }
}

TEST_CASE("Test intersection function") {
    double caffeineSolubility = 5.05;
    int caffeineWeight = 194;
    auto table1 = setupTest();
    REQUIRE(table1.AddEntry(test::column1.name, rowName, caffeineSolubility));
    std::string caffeineColour{"White"};
    REQUIRE(table1.AddEntry(test::column2.name, rowName, caffeineColour));
    REQUIRE(table1.AddEntry(test::column3.name, rowName, caffeineWeight));


    REQUIRE(table1.AddRow("Paracetamol"));
    double paracetamolSolubility = 4.97;
    int paracetamolWeight = 151;
    REQUIRE(table1.AddEntry(test::column1.name, "Paracetamol", paracetamolSolubility));
    std::string paracetamolColour{"White"};
    REQUIRE(table1.AddEntry(test::column2.name, "Paracetamol", paracetamolColour));
    REQUIRE(table1.AddEntry(test::column3.name, "Paracetamol", paracetamolWeight));

    auto columns2 = std::unordered_set<Column>({test::column1, test::column2});
    Row row2(columns2);
    Row row3(columns2);

    auto rows2 = Row::Map{{rowName, row2}, {"Phenylalanine", row3}};
    DataTable table2(columns2, rows2);

    double pnineSolubility = 3.3;
    std::string pnineColour{"Yellow"};
    REQUIRE(table2.AddEntry(test::column2.name, "Phenylalanine", pnineColour));
    REQUIRE(table2.AddEntry(test::column1.name, "Phenylalanine", pnineSolubility));

    SECTION("Two tables with some overlap") {
        REQUIRE(table2.AddEntry(test::column1.name, rowName, caffeineSolubility));
        REQUIRE(table2.AddEntry(test::column2.name, rowName, caffeineColour));

        auto testTable = DataTable::Intersection(table1, table2);

        REQUIRE(columns2 == testTable.GetColumns());

        auto compRow = row2;
        compRow.AddEntry(test::column1, caffeineSolubility);
        compRow.AddEntry(test::column2, caffeineColour);

        Row::Map compMap{{rowName, compRow}};
        REQUIRE(testTable.GetRows() == compMap);
    }

    SECTION("Two tables with no row overlap") {
        auto testTable = DataTable::Intersection(table1, table2);
        REQUIRE(columns2 == testTable.GetColumns());

        REQUIRE(testTable.GetRows().empty());
    }
}

TEST_CASE("Test Diff function") {
    double caffeineSolubility = 5.05;
    int caffeineWeight = 194;
    auto table1 = setupTest();
    REQUIRE(table1.AddEntry(test::column1.name, rowName, caffeineSolubility));
    std::string caffeineColour{"White"};
    REQUIRE(table1.AddEntry(test::column2.name, rowName, caffeineColour));
    REQUIRE(table1.AddEntry(test::column3.name, rowName, caffeineWeight));

    SECTION("Difference between two tables with some different columns and rows") {
        DataTable table2;
        table2.AddRow(rowName);
        std::string newRowName{"Phenylalanine"};
        table2.AddRow(newRowName);
        table2.AddColumn(test::column1);
        table2.AddColumn(test::column2);
        Column column{"Melting point", Column::Type::DECIMAL};
        table2.AddColumn(column);
        REQUIRE(table2.AddEntry(test::column1.name, rowName, caffeineSolubility));
        REQUIRE(table2.AddEntry(test::column2.name, rowName, caffeineColour));
        REQUIRE(table2.AddEntry(column.name, rowName, 515.9));
        REQUIRE(table2.AddEntry(test::column1.name, newRowName, 4.3));
        std::string plineColour{"brown"};
        REQUIRE(table2.AddEntry(test::column2.name, newRowName, plineColour));
        REQUIRE(table2.AddEntry(column.name, newRowName, 403.3));

        auto diffs = DataTable::Diff(table1, table2);
        REQUIRE(diffs.size() == 2);

        auto diff1 = diffs[0];
        REQUIRE(std::get<0>(diff1).data == table1.GetRow(rowName).value().data);
        REQUIRE(std::get<1>(diff1).data == table2.GetRow(rowName).value().data);
        auto caffeineDifferences = std::get<2>(diff1).Get();
        REQUIRE(caffeineDifferences.size() == 2);
        REQUIRE(std::get<int>(caffeineDifferences[0].first.value()) == caffeineWeight);
        REQUIRE(std::get<double>(caffeineDifferences[1].second.value()) == 515.9);

        auto diff2 = diffs[1];
        REQUIRE(std::get<0>(diff2).data.empty());
        REQUIRE(std::get<1>(diff2) == table2.GetRow(newRowName).value());
        auto plineDifferences = std::get<2>(diff2).Get();
        REQUIRE(plineDifferences.size() == 3);
    }

    SECTION("Difference between a table and itself") {
        REQUIRE(DataTable::Diff(table1, table1).empty());
    }
}
