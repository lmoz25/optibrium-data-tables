#pragma once

#include "Column.h"
namespace test {
    static Column column1{"Solubility", Column::Type::DECIMAL};
    static Column column2{"Colour", Column::Type::STRING};
    static Column column3{"Molecular Weight", Column::Type::INTEGER};
}    
