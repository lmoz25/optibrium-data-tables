#include <catch/catch.hpp>

#include "CommonTestData.h"

TEST_CASE("Testing column hash function") {
    Column column{"Solubility", Column::Type::DECIMAL};
    SECTION("Two columns with the same name should have the same hash") {
        REQUIRE(std::hash<Column>()(column) == std::hash<Column>()(test::column1));
    }

    SECTION("Test with std::unordered_set") {
        Column::Set columnSet{test::column1};
        auto res = columnSet.insert(column);
        REQUIRE(!res.second);
    }
}

TEST_CASE("Test equality operator") {
    SECTION("Check equal name means equal columns") {
        Column column1{"Colour", Column::Type::INTEGER};
        REQUIRE(column1 == test::column2);
    }

    SECTION("Check unequal name means unequal columns") {
        // Who needs a != operator right
        REQUIRE(!(test::column1 == test::column2));
    }
}

TEST_CASE("Test column set union") {
    SECTION("Two distinct columnsets") {
        Column::Set set1{{"Solubility", Column::Type::DECIMAL}};
        Column::Set set2{{"Molecular Weight", Column::Type::INTEGER}};
        auto testSet = Column::SetUnion(set1, set2);

        Column::Set compSet{{"Solubility", Column::Type::DECIMAL},
                            {"Molecular Weight", Column::Type::INTEGER}};
        REQUIRE(testSet == compSet);
    }

    SECTION("Columnsets with some overlap") {
        Column::Set set1{{"Solubility", Column::Type::DECIMAL},
                        {"Colour", Column::Type::STRING}};
        Column::Set set2{{"Solubility", Column::Type::DECIMAL},
                        {"Molecular Weight", Column::Type::INTEGER}};

        auto testSet = Column::SetUnion(set1, set2);

        Column::Set compSet{{"Solubility", Column::Type::DECIMAL},
                            {"Molecular Weight", Column::Type::INTEGER},
                            {"Colour", Column::Type::STRING}};

        REQUIRE(testSet == compSet);
    }

    SECTION("One set is superset of another") {
        Column::Set set1{{"Solubility", Column::Type::DECIMAL},
                        {"Colour", Column::Type::STRING}};
        Column::Set set2{{"Solubility", Column::Type::DECIMAL},
                            {"Molecular Weight", Column::Type::INTEGER},
                            {"Colour", Column::Type::STRING}};

        auto testSet = Column::SetUnion(set1, set2);
        REQUIRE(testSet == set2);
    }
}

TEST_CASE("Test column set intersection") {
    Column::Set set1{{"Solubility", Column::Type::DECIMAL},
                {"Colour", Column::Type::STRING}};

    SECTION("Test with some intersection") {
        Column::Set set2{{"Solubility", Column::Type::DECIMAL},
                            {"Molecular Weight", Column::Type::INTEGER}};

        auto testSet = Column::SetIntersection(set1, set2);

        Column::Set compSet{{"Solubility", Column::Type::DECIMAL}};
        REQUIRE(testSet == compSet);
    }

    SECTION("Test one superset of other") {
        Column::Set set2{{"Solubility", Column::Type::DECIMAL}};

        auto testSet = Column::SetIntersection(set1, set2);
        REQUIRE(testSet == set2);
    }

    SECTION("Test one superset of other") {
        Column::Set set2{{"Molecular Weight", Column::Type::INTEGER}};

        auto testSet = Column::SetIntersection(set1, set2);
        REQUIRE(testSet.empty());
    }
}