#include <catch/catch.hpp>

#include "CommonTestData.h"
#include "Difference.h"

TEST_CASE("Testing difference generator") {
    auto columns1 = Column::Set{test::column1, test::column2};
    Row row1{columns1};
    SECTION("Difference between two completely different rows") {
        auto columns2 = Column::Set{test::column3};
        Row row2{columns2};

        Difference diff{row1, row2};
        REQUIRE(!diff.RowsEqual());
        auto diffs = diff.Get();
        REQUIRE(diffs.size() == 3);
    }

    SECTION("Difference between two rows with the same columns but a different value") {
        Row row2{row1};
        row1.AddEntry(test::column1, 4.2);
        row2.AddEntry(test::column1, 8.9);
        std::string commonEntry{"White"};
        row1.AddEntry(test::column2, commonEntry);
        row2.AddEntry(test::column2, commonEntry);

        Difference diff{row1, row2};
        REQUIRE(!diff.RowsEqual());
        auto diffs = diff.Get();

        REQUIRE(diffs.size() == 1);
        REQUIRE(std::get<double>(diffs[0].first.value()) == 4.2);
        REQUIRE(std::get<double>(diffs[0].second.value()) == 8.9);
    }

    SECTION("Check no difference") {
        Difference diff{row1, row1};
        REQUIRE(diff.RowsEqual());
        REQUIRE(diff.Get().size() == 0);
    }
}