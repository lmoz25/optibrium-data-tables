#include <catch/catch.hpp>

#include "CommonTestData.h"
#include "Row.h"

Row testSetup() {
    Column::Set columns{};
    Row row{columns};
    REQUIRE(row.AddColumn(test::column1));
    return row;
}

TEST_CASE("Testing AddColumn function") {
    auto row = testSetup();
    SECTION("Add a column successfully") {
        REQUIRE(row.data.find(test::column1) != row.data.end());
    }

    SECTION("Try to add a column that already exists") {
        REQUIRE(!(row.AddColumn(test::column1)));
    }

    SECTION("Try to add a column with the same name but different type") {
        Column column{test::column1.name, Column::Type::NONE};
        REQUIRE(!(row.AddColumn(column)));
    }
}

TEST_CASE("Testing AddEntry function") {
    auto row = testSetup();
    SECTION("Add a valid entry") {
        auto testVal = 13.2;
        REQUIRE(row.AddEntry(test::column1, testVal));
        auto entry = row.data.find(test::column1)->second;
        REQUIRE(entry);
        REQUIRE(std::get<double>(entry.value()) == testVal);
    }
    SECTION("Fail to add an entry of the wrong type") {
        std::string entry{"Super soluble"};
        REQUIRE(!(row.AddEntry(test::column1, entry)));
    }
    SECTION("Fail to add an entry for a column not in this row") {
        std::string testVal{"White"};
        REQUIRE(!row.AddEntry(test::column2, testVal));
    }
}

TEST_CASE("Testing MapUnion function") {
    Column column1{"Solubility", Column::Type::DECIMAL};
    Column column2{"Molecular Weight", Column::Type::INTEGER};
    Column::Set columns{column1, column2};
    Row row1{columns};
    Row row2{columns};
    REQUIRE(row1.AddEntry(column1, 4.97));
    REQUIRE(row1.AddEntry(column2, 151));
    REQUIRE(row2.AddEntry(column1, 5.05));
    REQUIRE(row2.AddEntry(column2, 194));

    SECTION("Two distinct maps") {
        Row::Map map1{{"Paracetamol", row1}};
        Row::Map map2{{"Caffeine", row2}};Row row3{columns};
        REQUIRE(row3.AddEntry(column1, 0.4));
        REQUIRE(row3.AddEntry(column2, 358));
        auto testMap = Row::MapUnion(map1, map2);


        Row::Map compMap{{"Paracetamol", row1},
                        {"Caffeine", row2}};
        REQUIRE(testMap == compMap);
    }

    SECTION("Two maps with some overlap") {
        Row row3{columns};
        REQUIRE(row3.AddEntry(column1, 0.4));
        REQUIRE(row3.AddEntry(column2, 358));

        Row::Map map1{{"Paracetamol", row1},
                    {"Indomethacin", row3}};
        Row::Map map2{{"Caffeine", row2},
                    {"Indomethacin", row3}};

        auto testMap = Row::MapUnion(map1, map2);

        Row::Map compMap{{"Paracetamol", row1},
                        {"Caffeine", row2},
                        {"Indomethacin", row3}};
        REQUIRE(testMap == compMap);
    }

    SECTION("Two maps, one superset of other") {
        Row::Map map1{{"Paracetamol", row1}};
        Row::Map map2{{"Paracetamol", row1},
                    {"Caffeine", row2}};

        auto testMap = Row::MapUnion(map1, map2);

        REQUIRE(testMap == map2);
    }
}

TEST_CASE("Testing map intersection function") {
    Column column1{"Solubility", Column::Type::DECIMAL};
    Column column2{"Molecular Weight", Column::Type::INTEGER};
    Column::Set columns{column1, column2};
    Row row1{columns};
    Row row2{columns};
    REQUIRE(row1.AddEntry(column1, 4.97));
    REQUIRE(row1.AddEntry(column2, 151));
    REQUIRE(row2.AddEntry(column1, 5.05));
    REQUIRE(row2.AddEntry(column2, 194));

    Row::Map map1{{"Paracetamol", row1},
                    {"Caffeine", row2}};
    Row row3{columns};
    REQUIRE(row3.AddEntry(column1, 0.4));
    REQUIRE(row3.AddEntry(column2, 358));

    SECTION("Two maps with a row in common") {
        Row::Map map2{{"Paracetamol", row1},
                        {"Indomethacin", row3}};

        auto testMap = Row::MapIntersection(map1, map2);
        Row::Map compMap = {{"Paracetamol", row1}};

        REQUIRE(testMap == compMap);
    }

    SECTION("Two maps with no overlap") {
        Row::Map map2{{"Indomethacin", row3}};
        auto testMap = Row::MapIntersection(map1, map2);
        REQUIRE(testMap.empty());
    }

    SECTION("Two maps with the same row name but not same columns") {
        Column column3{"Colour", Column::Type::STRING};
        Column::Set columns2{column3};
        Row row4{columns2};
        Row::Map map2{{"Paracetamol", row4}};
        auto testMap = Row::MapIntersection(map1, map2);
        REQUIRE(testMap.empty());
    }
}