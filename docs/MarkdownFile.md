@mainpage Data Table

This is the documentation for the data table project, created for an Optibrium coding test by Liam Morris.

Descriptions of the classes and functions can be navigated to using the dropdown menus.