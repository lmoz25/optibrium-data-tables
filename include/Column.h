/// @file Column.h
#pragma once

#include <string>
#include <unordered_set>

/**
 * @brief Struct to represent a named column in the table.
 * 
 */
struct Column {
    /**
     * @brief Enum to represent the type of data that the column holds.
     * 
     */
    enum class Type {
        NONE,
        INTEGER,
        DECIMAL,
        STRING
    };
    /**
     * @brief Construct a new Column object.
     * 
     * @param name The name of the column.
     * @param type The type of data the column holds.
     */
    Column(const std::string name, const Type type) : name(name), type(type){} 

    typedef std::unordered_set<Column> Set;
    /**
     * @brief Perform a union between two sets of columns.
     * 
     * @param set1 
     * @param set2 
     * @return The union of the two sets.
     */
    static Column::Set SetUnion(const Column::Set& set1, const Column::Set& set2);

    /**
     * @brief Perform an intersection between two sets of columns.
     * 
     * @param set1 
     * @param set2 
     * @return The intersection of the two sets.
     */
    static Column::Set SetIntersection(const Column::Set& set1, const Column::Set& set2);

    /// @brief The name of the column.
    std::string name;
    /// @brief Its type.
    Type type;
};

/**
 * @brief Specialised hash for Column object, to support placing them in a Column::Set.
 * We use the string hash of the column name, because users shouldn't be able to define two columns
 * with the same name and different types.
 */
template<>
struct std::hash<Column> {
    size_t operator()(const Column& c) const noexcept
    {
        return std::hash<std::string>()(c.name);
    }
};

/**
 * @brief Equality operator.
 * Again, we take equality to mean two columns with the same name.
 * @param c1 
 * @param c2 
 * @return true 
 * @return false 
 */
inline bool operator==(const Column& c1, const Column& c2) {
    return c1.name == c2.name;
}