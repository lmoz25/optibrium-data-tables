/// @file Row.h
#pragma once

#include <variant>
#include <unordered_map>
#include <iostream>

#include "Column.h"

/**
 * @brief Struct to represent a row in the table.
 * 
 */
struct Row {
    /**
     * @brief Construct a new Row object
     * 
     */
    Row() = default;
    /**
     * @brief Construct a new Row object
     * 
     * @param columns A set of columns.
     */
    Row(Column::Set& columns);

    /// @brief Representation of an entry in the row: can an int, double, string or empty.
    typedef std::optional<std::variant<int, double, std::string>> Entry;

    /// @brief The data contained in the row, i.e a mapping of column to entry.
    std::unordered_map<Column, Entry> data;

    /**
     * @brief Function to add an entry to a column for this row.
     * 
     * @tparam T The type of the entry
     * @param column 
     * @param value 
     * @return true If insertion succeeds.
     * @return false If insertion fails, i.e if trying to insert a value of the wrong type
     * into the column or if the column doesn't exist.
     */
    template <typename T>
    bool AddEntry(const Column& column, const T& value) {
        if (!typeMatchesColumnType<T>(column.type)){
            return false;
        }
        auto colIter = data.find(column);
        if(colIter == data.end()) {
            return false;
        }
        Entry newEntry{value};
        std::swap(colIter->second, newEntry);
        return true;
    }
    /**
     * @brief Add a column to this row
     * 
     * @param column 
     * @return true On success
     * @return false On failure, usually if the column is already present.
     */
    bool AddColumn(const Column& column);
    /**
     * @brief Add a set of columns to this row
     * 
     * @param columns
     */
    void AddColumns(const Column::Set& columns);
    /**
     * @brief Remove a column from the row
     * 
     * @param columnName
     * @return true On success
     * @return false On failure, i.e if the column is not present in the row.
     */
    bool RemoveColumn(const std::string& columnName);
    /**
     * @brief Remove the entry at the given column
     * 
     * @param columnName 
     * @return true On success
     * @return false On failure, i.e if the column is not present in the row.
     */
    bool RemoveEntry(const std::string& columnName);
    /**
     * @brief Get a column of the given name
     * 
     * @param columnName 
     * @return std::optional<Column> Containing the column, if present, or empty otherwise.
     */
    std::optional<Column> GetColumn(const std::string& columnName);
    /**
     * @brief Get the Entry associated with the given column.
     * 
     * @param columnName 
     * @return Entry containing the entry associated with the given column, which is empty if the entry
     * in the column is empty or if the column doesn't exist.
     */
    Entry GetEntry(const std::string& columnName);

    /// @brief Row::Map associates rows with the names of the compounds they represent.
    typedef std::unordered_map<std::string, Row> Map;
    /**
     * @brief Take the union of two row maps.
     * 
     * @param map1 
     * @param map2 
     * @return Row::Map The union.
     */
    static Row::Map MapUnion(const Row::Map& map1, const Row::Map& map2);
    /**
     * @brief Take the intersection of two row maps.
     * 
     * @param map1 
     * @param map2 
     * @return Row::Map The intersection.
     */
    static Row::Map MapIntersection(const Row::Map& map1, const Row::Map& map2);
private:
    /**
     * @brief Merge two rows together, resulting in one row containing all columns and entries from both rows.
     * Where the two rows have a different entry for the same column, the entry from the row2 is used.
     * @param row1 
     * @param row2 
     * @return Row 
     */
    static Row merge(const Row& row1, const Row& row2);
    /**
     * @brief Checks that the input column contains the correct type.
     * Specialised for int, double and string only, as these are the three valid entry types.
     * 
     * @tparam T The C++ type we are trying to insert into the column.
     * @param type The Column::Type of the column.
     * @return true If the types match.
     * @return false Otherwise.
     */
    template <typename T>
    bool typeMatchesColumnType(const Column::Type& type);
};

/// @brief Equality operator for rows, i.e if the underlying columns/entries are the same.
inline bool operator==(const Row& row1, const Row& row2) {
    return row1.data == row2.data;
}

/// @brief Output ostream operator, used for debugging.
inline std::ostream& operator<<(std::ostream& os, const Row& row) {
    for (auto& colValIter : row.data) {
        os << colValIter.first.name << " ";
    }
    std::cout << std::endl;
    for (auto& colValIter : row.data) {
        if (colValIter.second) {
            auto outValVar = colValIter.second.value();
            switch (colValIter.first.type)
            {
            case Column::Type::DECIMAL :
                os << std::get<double>(outValVar) << " ";
                break;

            case Column::Type::INTEGER :
                os << std::get<int>(outValVar) << " ";
                break;

            case Column::Type::STRING :
                os << std::get<std::string>(outValVar);
                break;

            default:
                break;
            }
        }
        else {
            os << "-" << " ";
        }
    }
    return os;
}