/// @file DataTable.h
#pragma once

#include <memory>

#include "Difference.h"

/**
 * @brief Class to represent a data table. The table is made up of named rows and columns, and entries can be int,
 * double, string or empty.
 * 
 */
class DataTable {
public:
    /**
     * @brief Construct a new Data Table object
     * 
     */
    DataTable() = default;
    /**
     * @brief Construct a new Data Table object
     * 
     * @param columns A set of columns.
     * @param rows A list of named rows.
     */
    DataTable(Column::Set& columns, Row::Map& rows);
    /**
     * @brief Add an individual column to the table
     * 
     * @param column 
     * @return true On success.
     * @return false On failure, usually if the column is already present.
     */
    bool AddColumn(const Column& column);
    /**
     * @brief Add a row with the given name.
     * 
     * @param name 
     * @return true On success.
     * @return false On failure, usually if the row already exists.
     */
    bool AddRow(const std::string& name);
    /**
     * @brief Add the given entry for the given row at the given column.
     * 
     * @tparam T The C++ type of the entry to be added.
     * @param columnName 
     * @param rowName 
     * @param entry 
     * @return true On success.
     * @return false On failure, i.e if the row/column does not exist or if the entry is of the wrong type for the column.
     */
    template <typename T>
    bool AddEntry(const std::string& columnName, const std::string& rowName, const T& entry) {
        auto columnIterator = std::find_if(columns.begin(), columns.end(),
            [&](const Column& column) -> bool{
                return columnName == column.name;
            });

        if (columnIterator == columns.end()){
            // Column does not exists in table
            return false;
        }

        auto rowIterator = rows.find(rowName);
        if (rowIterator == rows.end()) {
            // Row does not exist
            return false;
        }

        auto column = *columnIterator;
        return rowIterator->second.AddEntry(column, entry);
    }
    /**
     * @brief Get the Entry at the given column/row.
     * 
     * @param columnName 
     * @param rowName 
     * @return Row::Entry 
     */
    Row::Entry GetEntry(const std::string& columnName, const std::string& rowName);

    /**
     * @brief Get the named Row, if it exists.
     * 
     * @param name 
     * @return std::optional<Row> Containing the row, empty if the row is not present.
     */
    std::optional<Row> GetRow(const std::string& name) const;
    /**
     * @brief Get the named Column, if it exists.
     * 
     * @param name 
     * @return std::optional<Column> Containing the column, empty if the column is not present.
     */
    std::optional<Column> GetColumn(const std::string& name) const;

    /**
     * @brief Get all the rows in the table, with their names (i.e the molecules associated with them)s
     * 
     * @return Row::Map 
     */
    Row::Map GetRows() const {
        return rows;
    }
    /**
     * @brief Get all the columns in the table.
     * 
     * @return Column::Set 
     */
    Column::Set GetColumns() const {
        return columns;
    }

    /**
     * @brief Remove the row associated with the named molecule.
     * 
     * @param name 
     * @return true On success.
     * @return false On failure, usually if the row is not present.
     */
    bool RemoveRow(const std::string& name);
    /**
     * @brief Remove the column with the given name.
     * 
     * @param name
     * @return true On success. 
     * @return false On failure, usually if the column is not present.
     */
    bool RemoveColumn(const std::string& name);
    /**
     * @brief Remove the entry at the named column/row
     * 
     * @param columnName 
     * @param rowName 
     * @return true On success.
     * @return false On failure, i.e if the row, column and/or entry doesn't exist
     */
    bool RemoveEntry(const std::string& columnName, const std::string& rowName);

    /**
     * @brief Take the union between two DataTables. Will construct a table with all the rows and columns that were present in the previous table, and insert blank entries to fill any gaps. 
     * 
     * If a row/column pair has a different entry in the two tables, the entry
     * from the second table is used.
     * 
     * @param table1 
     * @param table2 
     * @return DataTable 
     */
    static DataTable Union(const DataTable& table1, const DataTable& table2);
    /**
     * @brief Take the intersection between two DataTables. Will construct a table containing only data that is common between the two tables: if a row/column pair has a different entry in the two tables, the row/column pair will not be present in the constructed table.
     * 
     * @param table1 
     * @param table2 
     * @return DataTable 
     */
    static DataTable Intersection(const DataTable& table1, const DataTable& table2);
    /**
     * @brief Finds the difference between two data tables.
     * 
     * @param table1 
     * @param table2 
     * @return std::vector<std::tuple<Row, Row, Difference>> A vector containing tuples, where each tuple is (row_from_table1, row_from_table2, difference_between_two_rows).
     * Where a row is present in table 1 and not in table 2, the tuple will be (row_from_table1, empty_row, diff)
     */
    static std::vector<std::tuple<Row, Row, Difference>> Diff(
        const DataTable& table1, const DataTable& table2);

private:
    /**
     * @brief Having these as an `unordered_set/mapp` rather than `set/map` makes insertion, lookup and deletion operations
     * faster generally. It does suffer from not having STL set union algorithms, which is weird. For small tables, a
     * vector might be faster for Column::Set, but I'm assuming in practice the tables would be quite big, and not having to check for duplicates manually is nice.
     */
    Column::Set columns;
    Row::Map rows;
};