/// @file Difference.h
#pragma once

#include <tuple>
#include <vector>

#include "Row.h"

/**
 * @brief Class representing the difference between two rows in the table.
 * 
 */
class Difference {
public:
    /**
     * @brief Construct a new Difference object
     * 
     * @param r1 
     * @param r2 
     */
    Difference(const Row& r1, const Row& r2);
    /**
     * @brief Check if the two rows used to construct this difference are equal.
     * 
     * @return true If so.
     * @return false If not.
     */
    bool RowsEqual(){
        return rowsEqual;
    }
    /**
     * @brief Get the differences between the two rows found. This will return 
     * 
     * @return std::vector<std::pair<Row::Entry, Row::Entry>> A vector of pairs of entries, representing the
     * differences, in the order that the initial rows were input on construction. 
     * 
     * If the two rows have columns in common, the pair will show the entries for that column in each row.
     * For columns that exist in row1 but not row2, the first element of the pair will contain the entry in row1 at that
     * column, and the second element will be empty; and vice versa.
     */
    std::vector<std::pair<Row::Entry, Row::Entry>> Get() {
        return differences;
    }
private:
    /**
     * @brief Computes the differences between the two rows, and stores them in `differences`
     * 
     * @param r1 
     * @param r2 
     */
    void findDifferences(const Row& r1, const Row& r2);
    /// @brief Vector containing the entries in the two rows that differ.
    std::vector<std::pair<Row::Entry, Row::Entry>> differences;
    bool rowsEqual;
};